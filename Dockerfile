FROM node:alpine

ENV HOST localhost
ENV PORT 9090
ENV ROOT /srv/data/git/chix/chix-cdn-service
ENV TMPDIR /tmp/store_us_here_in_global_tmp
ENV REGISTRY https://registry.npmjs.org
# ENV OBJENV_PREFIX
ENV MEMCACHED_HOST localhost
ENV MEMCACHED_PORT 11211

ADD lib/ /app/lib
ADD package.json /app

WORKDIR /app

RUN apk --no-cache add --virtual native-deps \
 git g++ gcc libgcc libstdc++ linux-headers make python && \
 npm install node-gyp -g &&\
 npm install &&\
 npm rebuild bcrypt --build-from-source && \
 npm cache clean --force &&\
 apk del native-deps

USER node

EXPOSE 9000

ENTRYPOINT ["npm", "run", "start:prod"]
