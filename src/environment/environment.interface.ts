import { Response } from 'express'

export interface IEnvironment {
  host: string
  port: string
  root: string
  tmpDir: string
  registry: string

  memcached: {
    host: string
    port: string
  }

  npmInstallEnvVars: string[]
  debugEndpoints: boolean

  cors: {
    origin: string,
    methods: string,
    preflightContinue: boolean,
    optionsSuccessStatus: number
  }

  additionalBundleResHeaders: {
    [key: string]: string
  }

  onBadRequest: (response: Response) => void
  onError: (response: Response) => void
}
