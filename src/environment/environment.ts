import { resolve } from 'path'
import { IEnvironment } from './environment.interface'

const cacheExpiration = 60 * 60 * 24 * 365

// Should also stash this stuff in redis.
// and just keep it there. in memory makes no sense.
// the .tmp dir should be in memory.
// also the package should be removed after usage.
export const environment: IEnvironment = {
  host: 'localhost',
  port: '9000',
  // Used to resolve ./node_modules
  root: resolve(__dirname, '../../'),
  // Used as temporary storage, this should already be exact here, not determined in the code.
  // ok that works, but I have no clue why the dir should not be cleaned up afterwards.
  tmpDir: '/tmp/store_us_here_in_global_tmp',
  registry: 'https://registry.npmjs.org',
  npmInstallEnvVars: [],
  // npmInstallEnvVars: ["npm_config_cache=~/.npm"],
  debugEndpoints: true,
  memcached: {
    host: 'localhost',
    port: '14211'
  },
  cors: {
    origin: '*',
    // This one's locked down a little
    methods: 'GET,POST',
    preflightContinue: false,
    optionsSuccessStatus: 204
  },
  /*
  additionalBundleResHeaders: {
    'Cache-Control': 'max-age=86400'
  },
  */
  additionalBundleResHeaders: {
    'Cache-Control': 'public, max-age=' + cacheExpiration,
    'X-Content-Type-Options': 'nosniff',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Request-Method': 'GET',
    'Strict-Transport-Security': `max-age=${cacheExpiration}; includeSubDomains; preload`
  },
  onBadRequest (_res) {
    // res.status(200)
  },
  onError (_res) {
    // res.status(200)
  }
}
