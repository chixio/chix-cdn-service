import * as compression from 'compression'
import * as express from 'express'
import * as cors from 'cors'
import * as MemcachePlus from 'memcache-plus'

import { environment, IEnvironment } from './environment'
import { logger } from './pack/logger'
import { servePackage } from './pack/servePackage'
import { pkg } from './pkg'
import * as objenv from 'objenv'

(async () => {
  const config: IEnvironment = objenv(environment, {
    prefix: process.env.OBJENV_PREFIX || ''
  })

  const cache = new MemcachePlus(`${config.memcached.host}:${parseInt(config.memcached.port, 10)}`)

  const app = express()

  app.use(compression())
  app.use(servePackage(config, cache))

  app.use(cors(config.cors))

  await app.listen(config.port)

  logger.info(`${pkg.name}:${pkg.version} is online`)
  logger.info(`http://${config.host}:${config.port}`)
})()
