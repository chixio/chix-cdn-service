import { readJsonSync } from 'fs-extra'
import { resolve } from 'path'

export const pkg = readJsonSync(resolve(__dirname, '../package.json'))

