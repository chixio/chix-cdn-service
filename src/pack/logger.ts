import { createWriteStream } from 'fs-extra'
import * as minilog from 'minilog'
import { pkg } from '../pkg'

minilog.enable()
minilog.pipe( createWriteStream( `/tmp/${pkg.name}_log` ) )

export const logger = minilog( `${pkg.name}` )
