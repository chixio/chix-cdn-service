const LRU = require( 'lru-cache' )

export const cache = LRU({
	max: 128 * 1024 * 1024,
	length: src => src.length
})
