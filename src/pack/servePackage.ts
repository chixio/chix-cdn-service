import { get } from './utils/get'
import { sendBadRequest, sendError } from './utils/responses'
import { findVersion } from './utils/findVersion'
import { stringify } from './utils/stringify'
import { fetchBundle } from './fetchBundle'
import * as semver from 'semver'
import { readFileSync } from 'fs-extra'
import * as etag from 'etag'
import { logger } from './logger'
import { IEnvironment } from '../environment/environment.interface'
import { Request, Response, NextFunction } from 'express'

export function servePackage(config: IEnvironment, cache) {
  return (req: Request, res: Response, next: NextFunction) => {
    if ( req.method !== 'GET' ) return next()

    if (req.url === '/favicon.ico') {
      res.statusCode = 400
      res.end()

      return
    }

    const match = /^\/(?:@([^\/]+)\/)?([^@\/]+)(?:@(.+?))?(?:\/(.+?))?(?:\?(.+))?$/.exec( req.url )

    if ( !match ) {
      // TODO make this prettier

      return sendBadRequest(res, 'Invalid module ID' )
    }

    const user = match[1]
    const id = match[2]
    const tag = match[3] || 'latest'
    const deep = match[4]
    const queryString = match[5]

    const qualified = user ? `@${user}/${id}` : id
    const query = ( queryString || '' )
      .split( '&' )
      .reduce( ( query, pair ) => {
        if ( !pair ) return query

        const [ key, value ] = pair.split( '=' )
        query[ key ] = value || true
        return query
      }, {} )

    get( `${config.registry}/${encodeURIComponent( qualified ).replace('%40', '@')}` ).then( JSON.parse )
      .then( meta => {
        if ( !meta.versions ) {
          logger.error( `[${qualified}] invalid module` )

          return sendBadRequest(res, 'invalid module' )
        }

        const version = findVersion( meta, tag )

        if ( !semver.valid( version ) ) {
          logger.error( `[${qualified}] invalid tag` )

          return sendBadRequest(res, 'invalid tag' )
        }

        if ( version !== tag ) {
          let url = `/${meta.name}@${version}`
          if ( deep ) url += `/${deep}`
          url += stringify( query )

          // res.redirect( 302, url )
          res.setHeader('Location', url)
          res.statusCode = 301
          res.end()

          return
        }

        return fetchBundle( meta, tag, deep, query, cache ).then( zipped => {
          logger.info( `[${qualified}] serving ${zipped.length} bytes` )
          res.statusCode = 200
          res.setHeader('Content-Length', zipped.length)
          res.setHeader('Content-Type', 'application/javascript; charset=utf-8')
          res.setHeader(  'Content-Encoding', 'gzip')

          for ( const key in config.additionalBundleResHeaders) {
            if (config.additionalBundleResHeaders.hasOwnProperty(key)) {
              res.setHeader(  key, config.additionalBundleResHeaders[key])
            }
          }

          // FIXME(sven): calculate the etag based on the original content
          res.setHeader('ETag', etag(zipped))
          res.end( zipped )
        })
      })
      .catch( err => {
        logger.error( `[${qualified}] ${err.message}`, err.stack )
        const page = readFileSync( `${config.root}/server/templates/500.html`, { encoding: 'utf-8' })
          .replace( '__ERROR__', err.message )

        sendError(res, page)
      })
  }
}
