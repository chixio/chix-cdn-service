import { createBundle } from './createBundle'
import { gzipSync } from 'zlib'
import { logger } from './logger'
import { sha1 } from './utils'

const inProgress = {}

export async function fetchBundle (pkg, version, deep, query, cache) {
  let hash = `${pkg.name}@${version}`

  // it's being hashed, so why replace?
  // if ( deep ) hash += `_${deep.replace( /\//g, '_' )}`
  if (deep) hash += deep

  // ignoring the query. no custom names
  // hash += stringify( query )

  logger.info( `[${pkg.name}] requested package` )

  // hash = sha1(hash).toString()
  logger.info( `[${pkg.name}] pre-hash: ${hash}` )

  hash = sha1(hash)

  logger.info( `[${pkg.name}] hash: ${hash}` )

  const redKey = `chix-cdn:${hash}`

  const cachedItem = await cache.get(redKey)

  if ( cachedItem ) {
    logger.info( `[${pkg.name}] is cached` )

    return Buffer.from(cachedItem, 'base64')
  }

  if ( inProgress[ hash ] ) {
    logger.info( `[${pkg.name}] request was already in progress` )
  } else {
    logger.info( `[${pkg.name}] is not cached` )

    inProgress[ hash ] = createBundle( hash, pkg, version, deep, query )
      .then( result => {
        const zipped = gzipSync( result )

        return cache.set( redKey , zipped.toString('base64') )
          .then(() => zipped)
      }, err => {
        inProgress[ hash ] = null

        throw err
      })
      .then( zipped => {
        inProgress[ hash ] = null

        return zipped
      })
  }

  return inProgress[ hash ]
}
