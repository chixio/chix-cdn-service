import { environment } from '../../environment'

export function sendBadRequest (res, msg) {
	res.statusCode = 400

	if (typeof environment.onBadRequest === 'function') {
		environment.onBadRequest(res)
	}

	res.end( msg )
}

export function sendError (res, msg) {
	res.statusCode = 500

	if (typeof environment.onError === 'function') {
	  environment.onError(res)
	}

	res.end( msg )
}
