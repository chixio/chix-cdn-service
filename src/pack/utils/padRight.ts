export function padRight ( str: string, num: number, char: string = ' ' ): string {
	while ( str.length < num ) str += char

	return str
}
