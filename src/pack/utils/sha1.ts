import { createHash } from 'crypto'

export const sha1 =( data ) =>
  createHash('sha1')
    .update( data )
    .digest('hex')
