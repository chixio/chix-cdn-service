import { exec } from 'child_process'

export function directorySize(directory: string, human: boolean = true): Promise<string> {
  const h = human ? ' -h' : ''

  return new Promise(
    (resolve, reject) => {
      exec( 	`du${h} -d 0 ${directory} | cut -f 1`	, (error, stdout) => {
        if(!error) {
          resolve(stdout.trim())
        } else {
          reject(error)
        }
      })
    }
  )
}
