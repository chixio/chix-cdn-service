import * as semver from  'semver'

export function findVersion ( meta: any, tag: string ): string {
	// already a valid version?
	if ( semver.valid( tag ) ) return meta.versions[ tag ] && tag

	// dist tag
	if ( tag in meta[ 'dist-tags' ] ) return meta[ 'dist-tags' ][ tag ]

	// semver range
	return semver.maxSatisfying( Object.keys( meta.versions ), tag )
}
