import * as https from  'https'

export function get( url: string ): Promise<string> {
	return new Promise( ( resolve, reject ) => {
		https.get( url, response => {
			let body = ''

			response.on( 'data', chunk => {
				body += chunk
			})

			response.on( 'end', () => {
				resolve( body )
			})

			response.on( 'error', reject )
		})
	})
}
