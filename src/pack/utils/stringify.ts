export function stringify ( query ): string {
  const str = Object.keys( query ).sort().map( key => `${key}=${query[key]}` ).join( '&' )

  return str ? `?${str}` : ''
}
