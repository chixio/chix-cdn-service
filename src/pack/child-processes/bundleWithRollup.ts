import { info } from './info'
import * as path from 'path'
import * as rollup from 'rollup'
import * as resolve from  'rollup-plugin-node-resolve'
import { bundleWithBrowserify } from './bundleWithBrowserify'

export async function bundleWithRollup (cwd: string, pkg: any, moduleEntry: string, moduleName: string ): Promise<string> {
  const bundle = await rollup.rollup({
    entry: path.resolve( cwd, moduleEntry ),
    plugins: [
      resolve({ module: true, jsnext: true, main: false, modulesOnly: true })
    ]
  })

  info( `[${pkg.name}] bundled using Rollup` )

  if ( bundle.imports.length > 0 ) {
    info( `[${pkg.name}] non-ES2015 dependencies found, handing off to Browserify` )

    const intermediate = `${cwd}/__intermediate.js`

    await bundle.write({
      dest: intermediate,
      format: 'cjs'
    })

    return bundleWithBrowserify( pkg, intermediate, moduleName )
  }

  const { code } = await bundle.generate({
    format: 'umd',
    moduleName
  })

  return code
}
