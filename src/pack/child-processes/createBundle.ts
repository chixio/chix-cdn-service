import { mkdirp, remove } from 'fs-extra'
import { fetchAndExtract } from './fetchAndExtract'
import { sanitizePkg } from './sanitizePkg'
import { installDependencies } from './installDependencies'
import { bundle } from './bundle'
import { info } from './info'
import { minify } from  'uglify-js'
import { IEnvironment } from '../../environment'
import { resolve } from 'path'

export function createBundle (config: IEnvironment) {
  return async ({ hash, pkg, version, deep, query }): Promise<void> => {
    const dir = `${config.tmpDir}/${hash}`
    const cwd = resolve(`${dir}/package`)

    try {
      await mkdirp( dir )
      await fetchAndExtract( pkg, version, dir )
      await sanitizePkg( cwd )
      await installDependencies( cwd, config )

      const code = await bundle( cwd, deep, query, hash )

      await remove( dir )

      info( `[${pkg.name}] minifying` )

      const result = minify(code, {
        mangle: {
          keep_fnames: true
        } 
      })

      if ( result.error ) {
        info( `[${pkg.name}] minification failed: ${result.error.message}` )
      }

      process.send({
        type: 'result',
        code: result.error ? code : result.code
      })
    } catch ( err ) {
      await remove( dir )

      process.send({
        type: 'error',
        message: err.message,
        stack: err.stack
      })
    }
  }
}
