export interface InfoMessage {
  type: 'info'
  message: string
}

export function info ( message: string ): void {
  process.send({
    type: 'info',
    message
  })
}
