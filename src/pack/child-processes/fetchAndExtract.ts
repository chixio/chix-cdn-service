import { info } from './info'
import { createWriteStream } from 'fs-extra'
import * as tar from 'tar'
import * as request from 'request'

export function fetchAndExtract (pkg: any, version: string, dir: string ): Promise<void> {
  const tarUrl = pkg.versions[ version ].dist.tarball

  info( `[${pkg.name}] fetching ${tarUrl}` )

  return new Promise( ( resolve, reject ) => {
    let timedOut = false

    const timeout = setTimeout( () => {
      reject( new Error( 'Request timed out' ) )
      timedOut = true
    }, 10000 )

    const input = request( tarUrl )

    // don't like going via the filesystem, but piping into targz
    // was failing for some weird reason
    const intermediate = createWriteStream( `${dir}/package.tgz` )

    input.pipe( intermediate )

    intermediate.on( 'close', () => {
      clearTimeout( timeout )

      if ( !timedOut ) {
        info( `[${pkg.name}] extracting to ${dir}/package` )

        tar
          .x({
            file: `${dir}/package.tgz`,
            cwd: dir,
          })
          .then( resolve, reject )
      }
    })
  })
}
