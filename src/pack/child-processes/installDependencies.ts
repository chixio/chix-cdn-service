import { info } from './info'
import { exec } from './exec'
import { IEnvironment } from '../../environment'

export function installDependencies (cwd: string, config: IEnvironment ): Promise<void> {
  const pkg = require( `${cwd}/package.json` )

  const envVariables = config.npmInstallEnvVars.join(' ')
  const installCommand = `${envVariables} ${config.root}/node_modules/.bin/npm install --production`

  info( `[${pkg.name}] running ${installCommand}` )

  return exec( installCommand, cwd, pkg ).then( () => {
    if ( !pkg.peerDependencies ) return

    return Object.keys( pkg.peerDependencies ).reduce( ( promise, name ) => {
      return promise.then( () => {
        info( `[${pkg.name}] installing peer dependency ${name}` )
        const version = pkg.peerDependencies[ name ]

        return exec( `${config.root}/node_modules/.bin/npm install "${name}@${version}"`, cwd, pkg )
      })
    }, Promise.resolve())
  })
}
