import { findEntry } from './findEntry'
import { makeLegalIdentifier } from '../utils'
import { readFileSync } from 'fs-extra'
import { bundleWithRollup } from './bundleWithRollup'
import { bundleWithBrowserify } from './bundleWithBrowserify'
import { info } from './info'
import * as path from 'path'
import * as isModule from 'is-module'
import { sha1 } from '../utils/sha1'

export type PackageQuery = {
  name?: string
}

export function bundle (cwd: string, deep: string, query: PackageQuery, hash: string ) {
  const pkg = require( `${cwd}/package.json` )
  // const moduleName = 'x' + hash // query.name || makeLegalIdentifier( pkg.name )
  // If the browser requests multiple versions this will overwrite, but that's ok for now.
  const moduleName = 'x' + sha1(pkg.name) // query.name || makeLegalIdentifier( pkg.name )

  const entry = deep ?
    path.resolve( cwd, deep ) :
    findEntry( path.resolve( cwd, ( pkg.module || pkg[ 'jsnext:main' ] || pkg.main || 'index.js' ) ) )

  const code = readFileSync( entry, { encoding: 'utf-8' })

  if ( isModule( code ) ) {
    info( `[${pkg.name}] ES2015 module found, using Rollup` )

    return bundleWithRollup( cwd, pkg, entry, moduleName )
  }

  info( `[${pkg.name}] No ES2015 module found, using Browserify` )

  return bundleWithBrowserify( pkg, entry, moduleName )
}
