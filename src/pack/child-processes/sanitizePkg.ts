import { readJson, writeJson } from 'fs-extra'

export async function sanitizePkg (cwd: string ): Promise<void> {
  const pkg = await readJson(`${cwd}/package.json`)

  if (pkg) {
    pkg.scripts = {}

    return writeJson( `${cwd}/package.json`, pkg, {
      spaces: 2
    })
  }

  throw Error('Unable to find pkg')
}
