import { statSync } from 'fs-extra'

export function findEntry (file: string) {
  try {
    const stats = statSync(file)

    if ( stats.isDirectory() ) return `${file}/index.js`

    return file
  } catch (err) {
    return `${file}.js`
  }
}
