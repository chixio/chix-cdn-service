import * as browserify from 'browserify'
import { info } from './info'

export function bundleWithBrowserify ( pkg: any, main: string, moduleName: string ): Promise<string> {
  const b = browserify( main, {
    standalone: moduleName
  })

  return new Promise( ( resolve, reject ) => {
    b.bundle( ( err, buf ) => {
      if ( err ) {
        reject( err )
      } else {
        info( `[${pkg.name}] bundled using Browserify` )

        resolve( '' + buf )
      }
    })
  })
}
