import * as child_process from  'child_process'
import { info } from './info'

export function exec ( cmd: string, cwd: string, pkg: any ): Promise<void> {
  return new Promise( ( resolve, reject ) => {
    child_process.exec( cmd, { cwd }, ( err, stdout, stderr ) => {
      if ( err ) {
        return reject( err )
      }

      stdout.split( '\n' ).forEach( line => {
        info( `[${pkg.name}] ${line}` )
      })

      stderr.split( '\n' ).forEach( line => {
        info( `[${pkg.name}] ${line}` )
      })

      resolve()
    })
  })
}
