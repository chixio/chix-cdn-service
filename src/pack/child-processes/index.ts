import { createBundle } from './createBundle'
import * as objenv from 'objenv'
import { environment } from '../../environment'
import { logger } from '../logger'

const config = objenv(environment, {
  prefix: process.env.OBJENV_PREFIX || ''
})

const bundle = createBundle(config)

process.on( 'message', message => {
  logger.info(`Received ${message.type} message.`)
	if ( message.type === 'start' ) {
    bundle( message.params )
      .then(() => {
        logger.info(`Bundle done.`)
      })
	}
})

logger.info(`Sending ready message.`)
process.send( 'ready' )


















