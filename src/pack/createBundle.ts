import { fork } from 'child_process'
import { logger } from './logger'

export interface StartParams {
  hash: string
  pkg: string
  version: string
  deep: string
  query: string
}

export interface StartMessage {
  type: 'start'
  params: StartParams
}

export function createBundle (
  hash: string,
  pkg: string,
  version: string,
  deep: string,
  query: string
): Promise<string> {
  return new Promise( ( resolve, reject ) => {
    const child = fork( 'lib/pack/child-processes/index.js' )

    child.on( 'message', message => {
      if ( message === 'ready' ) {
        const startMessage: StartMessage = {
          type: 'start',
          params: {
            hash,
            pkg,
            version,
            deep,
            query
          }
        }

        child.send(startMessage)
      }

      if ( message.type === 'info' ) {
        logger.info( message.message )
      } else if ( message.type === 'error' ) {
        const error = new Error( message.message )

        error.stack = message.stack

        reject( error )

        child.kill()
      } else if ( message.type === 'result' ) {
        resolve( message.code )

        child.kill()
      }
    })
  })
}
